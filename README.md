# cognito-auth-sample

Replace `CLIENT_ID` and `CLIENT_SECRET` constants in `src/auth.js` before running.

```shell
git clone https://gitlab.com/danderson00/cognito-auth-sample.git
cd cognito-auth-sample
yarn
node src/host.js &
yarn start
```