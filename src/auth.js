const { createHmac } = require('crypto')
const { CognitoIdentityProviderClient, InitiateAuthCommand } = require("@aws-sdk/client-cognito-identity-provider")

const CLIENT_SECRET = 'CLIENT_SECRET'
const CLIENT_ID = 'CLIENT_ID'

const client = new CognitoIdentityProviderClient()

function generateHash(username) {
  const hasher = createHmac('sha256', CLIENT_SECRET)
  hasher.update(`${username}${CLIENT_ID}`)
  return hasher.digest('base64')
}

module.exports = async function authenticate(username, password) {
  const input = {
    AuthFlow: "USER_PASSWORD_AUTH",
    AuthParameters: {
      "USERNAME": username,
      "PASSWORD": password,
      "SECRET_HASH": generateHash(username)
    },
    ClientId: CLIENT_ID
  }
  const command = new InitiateAuthCommand(input)
  return client.send(command)
}
