const express = require('express')
const cors = require('cors')
const authenticate = require('./auth')

const app = express()
app.use(cors(), express.json())
app.post('/token', (req, res) => {
  authenticate(req.body.username, req.body.password)
    .then(result => {
      console.log(result)
      res.send({ success: true, ...result })
    })
    .catch(error => {
      res.status(400).send({ message: error.message })
    })
})
app.listen(4000)