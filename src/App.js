import { useState } from 'react'

export function App() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [message, setMessage] = useState('')

  const signIn = () => {
    fetch('http://localhost:4000/token', {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(res => res.json())
      .then(result => {
        console.log(result)
        setMessage(result.success ? 'success' : result.message)
      })
      .catch(error => setMessage(error.message))
  }

  return (
    <div>
      <div>Username: <input value={username} onChange={e => setUsername(e.target.value)} /></div>
      <div>Password: <input value={password} onChange={e => setPassword(e.target.value)} /></div>
      <div><button onClick={signIn}>Sign In</button></div>
      <div>{message}</div>
    </div>
  )
}
